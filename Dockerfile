FROM ubuntu:20.04
#!/bin/sh
# This runs on the Ubuntu instance where SIMH will be run
#
# This is copied onto the instance by the "launch-pdp-11.sh" script

# update the OS
# get dependencies for building SIMH
# build SIMH

# follow http://gunkies.org/wiki/Installing_Unix_v6_(PDP-11)_on_SIMH

# do a set of boots
# tape boot (tboot.ini) which will build the file systems and l

# must run as root, obviously


# get a bootable UNIX v6 PDP-11 tape image, need it for later
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get upgrade && DEBIAN_FRONTEND=noninteractive apt-get install --yes --no-install-recommends wget build-essential unzip libsdl2-dev libpcap-dev && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN wget --no-check-certificate http://sourceforge.net/projects/bsd42/files/Install%20tapes/Research%20Unix/Unix-v6-Ken-Wellsch.tap.bz2 && bunzip2 *.bz2 && mv Unix-v6-Ken-Wellsch.tap /root/dist.tap

# Begin with the Google standard Ubuntu 18.04 LTS (not the minimal)
#
# this should have been done by the instance creation script, but just in case...
# if you don't do this then a lot of the application packages won't install properly
# beside, its always a good idea to update+upgrade immediately after installation

# you will need these packages to build the emulator
# this loads about a gazillion packages, so go get a beer

#get the simh package and unpack
RUN wget --no-check-certificate https://github.com/simh/simh/archive/master.zip && unzip master.zip && cd simh-master && make pdp11 && cp BIN/pdp11 /usr/bin/ && cd .. && rm -rf master.zip simh-master
COPY *.ini /root/
WORKDIR /root
RUN pdp11 tboot.ini
RUN pdp11 buildunix.ini

EXPOSE 5555
ENTRYPOINT ["/usr/bin/pdp11",  "/root/normalboot.ini"]




